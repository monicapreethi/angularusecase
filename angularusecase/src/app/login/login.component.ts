import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { User } from '../user';
import { Router } from '@angular/router';
import { CurrentuserService } from '../currentuser.service';
import { UserService } from '../user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  users:User[]=[];
  user:User=new User();
    submitted = false;
    constructor(private formBuilder: FormBuilder,public us:UserService, private router:Router,private currentUser:CurrentuserService) { }

  ngOnInit():void { 
     }
  loginForm= new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', [Validators.required, Validators.minLength(6)])
  });
  get f() { return this.loginForm.controls; }
  /* onLoginPress(){
   
  } */
  onSubmit() {
    this.us.getUsers().subscribe((response)=>{
      this.users=response;
      console.log (this.users)
          this.submitted = true;
    let usrname = this.loginForm.get('username').value;
    let pwd=this.loginForm.get('password').value;
    for( this.user of this.users){
      
      if((usrname==this.user.username)&&(pwd==this.user.password)){
        console.log(this.user.username);
        console.log(this.user.password);
        if(this.user.usertype=='user'){
          this.currentUser.setUserData(this.user);
          this.user.isactive=true;
          this.router.navigate(['user']);
        }
        if(this.user.usertype=='admin'){
          this.currentUser.setUserData(this.user);
          this.user.isactive=true;
          this.router.navigate(['admin']);
        }
        if((this.user.usertype!='user')&&(this.user.usertype!='admin')){
          alert("admin will verify please wait");
        }
      }
      
    }
  });
    if (this.loginForm.invalid) {
      
        alert("please provide correct login credentials");
     
        return;
    }
  }
register(){
  this.router.navigate(['register']);
}
}
