import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { User } from '../user';
import{Location} from '@angular/common';
import { UserService } from '../user.service';


@Component({
  selector: 'app-updateuser',
  templateUrl: './updateuser.component.html',
  styleUrls: ['./updateuser.component.css']
})
export class UpdateuserComponent implements OnInit {


  user: User = new User();
  user1: User = new User();


  constructor(private router: Router, private us: UserService,private router1:Location) { }
  editCustomer() {
    
  }
  ngOnInit() :void{
    
  }
  onUpdate() {
    console.log("into update");
    console.log(this.user.id+","+this.user.username+","+this.user.emailid+","+this.user.phoneno+","+this.user.bankaccountnumber);
    let id = localStorage.getItem("id");
    this.us.getUser(+id).subscribe(data => {
      this.user1 = data;
      this.user.id=this.user1.id;
     this.user.bankaccountnumber=this.user.bankaccountnumber;
     this.user.balance=this.user.balance;
     this.user.isactive=this.user.isactive;
     this.user.usertype=this.user.usertype;
     this.user.username=this.user1.username;
     this.user.password=this.user1.password;
     this.user.emailid=this.user1.emailid;
     this.user.phoneno=this.user1.phoneno;
     
     this.us.update(this.user.id, this.user).subscribe(data => { console.log(data); }, error => console.log(error));
     this.router.navigate(['userlist']);

    })
    

  }
  navBack(){
    this.router1.back();
  }

}