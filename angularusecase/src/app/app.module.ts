import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { RegistrationComponent } from './registration/registration.component';



import { AccountComponent } from './account/account.component';


import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { UserService } from './user.service';
import { AdminComponent } from './admin/admin.component';
import { userComponent } from './user/user.component';

import { TransactionService } from './transaction.service'
import { CurrentuserService } from './currentuser.service';
import { TransactionlistComponent } from './transactionlist/transactionlist.component';
import { UpdateuserComponent } from './updateuser/updateuser.component';
import { ViewUserDetailsComponent } from './view-user-details/view-user-details.component';

import { TransferComponent } from './transfer/transfer.component';
const appRoutes : Routes =[
  {path:'',component:HomeComponent},
  {path:'login',component:LoginComponent},
  {path:'register',component:RegistrationComponent},
  {path:'user',component:userComponent},
  {path:'admin',component:AdminComponent},
  {path:'user/transfer',component:TransferComponent},
  {path:'admin/transfer',component:TransferComponent},
  {path:'user/account',component:AccountComponent},
  {path:'admin/account',component:AccountComponent},
  {path:'user/details',component:ViewUserDetailsComponent},
  {path:'admin/details',component:ViewUserDetailsComponent},
  {path:'admin/transactionslist',component:TransactionlistComponent},
  
  {path:'edit',component:UpdateuserComponent},
  
  
];
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    RegistrationComponent,
    
    UpdateuserComponent,
    TransactionlistComponent,
    ViewUserDetailsComponent,
    userComponent,
    
    AccountComponent,
    AdminComponent,
    TransferComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule

    

  
    
  ],
  providers: [UserService,TransactionService,CurrentuserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
