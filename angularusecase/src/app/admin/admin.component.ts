import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CurrentuserService } from 'src/app/currentuser.service';
import { User } from 'src/app/user';
import { UserService } from '../user.service';


@Component({
  selector: 'admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  user:User=new User();
  users;
  user1:User=new User();
  user2:User=new User();
  constructor(private currentUser:CurrentuserService,private router:Router,public us:UserService) { 
    console.log(currentUser.getUserData());
    this.user=currentUser.getUserData();
    currentUser.setUserData(this.user);
  }
  
  ngOnInit(): void {
    this.users = this.us.getUsers();
  }
 
  editUser(user: any): void {
   
    localStorage.setItem("id", user.id.toString());
    this.router.navigate(["edit"]);
  }
  logout(){
    this.currentUser.setUserData(null);
    this.user.isactive=false;
    this.us.update(this.user.id,this.user).subscribe();
    this.router.navigate(["login"]);
  }
  
}